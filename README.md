# Den

Den is a simple script to create LUKS container in file.

## Usage

```
Usage:
	den <command> [<args>...]
	den [options]

Description:
	Wrapper arround some Linux tools to easily create a LUKS container.

Commands:
	new	Create a new LUKS container.
	open	Open a LUKS container.
	close	Close a LUKS container.

Options:
	-h, --help	Display this message.
	-v, --version	Display version number.
```

### Create a new container

```
Create a new LUKS container.

Usage:
	den new [options]

new options:
	-f, --file <arg>		Path to the file to use for the container. (default: den.img)
					⚠️  If the file exist, all data will be erased! ⚠️
	-fs, --file-system <arg>	File system used to format the container. (default: ext4)
	-s, --size <arg>		Size of the new container. (default: 512M)
					A binary prefix must be specified. It can be: K, M or G.

Common options:
	-h, --help			Display this message.
```

### Open a container

```
Open a LUKS container. The program will try to mount and decrypt the specified container.

Usage:
	den open <container name>
	den open [options]

open options:
	-f, --file <arg>	Path to the container file to open. (default: den.img)
	-s, --slot <arg>	Slot number to use. This is a value between [0-9]. (default: 0)

Common options:
	-h, --help		Display this message.
```

### Close a container

```
Close a LUKS container. The script will first unmount the container and close it afterward.

Usage:
	den close <volume name>
	den close [options]

close options:
	-s, --slot	Slot number to close. This is a number between [0-9]. (default: 0)

Common options:
	-h, --help	Display this message.
```
